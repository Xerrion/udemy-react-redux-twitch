import React from "react";
import { BrowserRouter, Link, Route } from "react-router-dom";
import StreamList from "./streams/StreamList";
import StreamCreate from "./streams/StreamCreate";
import StreamDelete from "./streams/StreamDelete";
import StreamEdit from "./streams/StreamEdit";
import StreamShow from "./streams/StreamShow";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Route path={"/"} exact component={StreamList} />
        <Route path={"/streams/create"} component={StreamCreate} />
        <Route path={"/streams/delete"} component={StreamDelete} />
        <Route path={"/streams/edit"} component={StreamEdit} />
        <Route path={"/streams/show"} component={StreamShow} />
        <Link to={"/"}>StreamList</Link>
        <Link to={"/streams/create"}>StreamCreate</Link>
        <Link to={"/streams/delete"}>StreamDelete</Link>
        <Link to={"/streams/edit"}>StreamEdit</Link>
        <Link to={"/streams/show"}>StreamShow</Link>
      </BrowserRouter>
    </div>
  );
};
export default App;
